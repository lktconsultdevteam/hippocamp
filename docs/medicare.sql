-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema medicare
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema medicare
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `medicare` DEFAULT CHARACTER SET latin1 ;
USE `medicare` ;

-- -----------------------------------------------------
-- Table `medicare`.`addresses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`addresses` (
  `address_id` INT(11) NOT NULL AUTO_INCREMENT,
  `object_id` INT(11) UNSIGNED NOT NULL,
  `object_name` VARCHAR(20) NOT NULL,
  `address1` VARCHAR(40) NULL,
  `address2` VARCHAR(40) NULL DEFAULT NULL,
  `address3` VARCHAR(40) NULL DEFAULT NULL,
  `city` VARCHAR(40) NULL DEFAULT NULL,
  `state` VARCHAR(40) NULL DEFAULT NULL,
  `country` VARCHAR(40) NULL DEFAULT NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT '1',
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE INDEX `address_ok` (`object_id` ASC, `is_active` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`tenants`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`tenants` (
  `tenant_id` INT(11) UNSIGNED NOT NULL,
  `company` VARCHAR(40) NULL DEFAULT NULL,
  `address_id` INT(11) NOT NULL,
  `primary_contact` VARCHAR(40) NULL DEFAULT NULL,
  `email` VARCHAR(40) NULL DEFAULT NULL,
  `notes` TEXT NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`tenant_id`),
  INDEX `address` (`address_id` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`patients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`patients` (
  `patient_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_id` INT(11) UNSIGNED NOT NULL,
  `title` VARCHAR(40) NULL DEFAULT NULL,
  `first_name` VARCHAR(40) NULL DEFAULT NULL,
  `last_name` VARCHAR(40) NULL DEFAULT NULL,
  `gender` ENUM('Male','Femail') NULL DEFAULT 'Male',
  `dob` DATE NULL DEFAULT NULL,
  `address_id` INT(11) NOT NULL,
  `occupation` VARCHAR(80) NULL DEFAULT NULL,
  `referral_id` INT(11) NULL DEFAULT NULL,
  `reference_no` VARCHAR(40) NULL DEFAULT NULL,
  `ec_name` VARCHAR(40) NULL DEFAULT NULL,
  `ec_phone` VARCHAR(40) NULL DEFAULT NULL,
  `ec_email` VARCHAR(40) NULL DEFAULT NULL,
  `ec_relationship` VARCHAR(40) NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  INDEX `tenant_id` (`tenant_id` ASC),
  INDEX `patient_name` (`first_name` ASC, `last_name` ASC),
  INDEX `address` (`address_id` ASC),
  CONSTRAINT `fk_patients_tenants1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `medicare`.`tenants` (`tenant_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`admissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`admissions` (
  `admission_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` INT(11) UNSIGNED NOT NULL,
  `admission_date` DATE NULL DEFAULT NULL,
  `discharge_date` DATE NULL DEFAULT NULL,
  `service_type` VARCHAR(5) NOT NULL,
  `is_paid` TINYINT(1) NULL DEFAULT '0',
  `date_payment` DATETIME NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`admission_id`),
  INDEX `patient_id` (`patient_id` ASC),
  CONSTRAINT `fk_admissions_patients1`
  FOREIGN KEY (`patient_id`)
  REFERENCES `medicare`.`patients` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`claimants`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`claimants` (
  `claimant_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `medicare_num` VARCHAR(10) NULL DEFAULT NULL,
  `medicare_ref` VARCHAR(100) NULL DEFAULT NULL,
  `first_name` VARCHAR(40) NULL DEFAULT NULL,
  `last_name` VARCHAR(40) NULL DEFAULT NULL,
  `address_id` INT(11) NOT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  INDEX `medicare_num` (`medicare_num` ASC),
  INDEX `claimant_name` (`first_name` ASC, `last_name` ASC),
  INDEX `address` (`address_id` ASC),
  PRIMARY KEY (`claimant_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`claimant_accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`claimant_accounts` (
  `claimant_id` INT(11) UNSIGNED NOT NULL,
  `account_name` VARCHAR(30) NULL DEFAULT NULL,
  `account_number` INT(11) UNSIGNED NULL DEFAULT NULL,
  `account_bsb` VARCHAR(6) NULL DEFAULT NULL,
  `is_active` TINYINT(1) NULL DEFAULT '1',
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  UNIQUE INDEX `claimant_account_ok` (`claimant_id` ASC, `account_name` ASC, `account_number` ASC, `is_active` ASC),
  CONSTRAINT `fk_claimant_accounts_claimants`
  FOREIGN KEY (`claimant_id`)
  REFERENCES `medicare`.`claimants` (`claimant_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`claims`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`claims` (
  `claims_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `claimant_id` INT(11) UNSIGNED NOT NULL,
  `patient_id` INT(11) UNSIGNED NOT NULL,
  `claim_type` VARCHAR(5) NULL DEFAULT NULL,
  `charge_amount` FLOAT NULL DEFAULT NULL,
  `date_claims` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`claims_id`),
  UNIQUE INDEX `claims_ok` (`claimant_id` ASC, `patient_id` ASC, `date_claims` ASC),
  INDEX `fk_claims_patients1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_claims_claimants1`
  FOREIGN KEY (`claimant_id`)
  REFERENCES `medicare`.`claimants` (`claimant_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_claims_patients1`
  FOREIGN KEY (`patient_id`)
  REFERENCES `medicare`.`patients` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`lk_claim_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`lk_claim_types` (
  `claim_type_id` INT(11) UNSIGNED NOT NULL,
  `claim_code` VARCHAR(2) NOT NULL,
  `claim_name` VARCHAR(30) NOT NULL,
  `is_active` TINYINT(1) UNSIGNED NULL DEFAULT '1',
  PRIMARY KEY (`claim_type_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`lk_note_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`lk_note_type` (
  `note_type_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_id` INT(11) NULL DEFAULT NULL,
  `is_file` TINYINT(4) NULL DEFAULT '0',
  `note_type_desc` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`note_type_id`),
  INDEX `tenant_id` (`tenant_id` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`lk_service_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`lk_service_types` (
  `service_type_id` INT(11) UNSIGNED NOT NULL,
  `service_code` VARCHAR(2) NOT NULL,
  `service_name` VARCHAR(30) NOT NULL,
  `is_active` TINYINT(1) UNSIGNED NULL DEFAULT '1')
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `medicare`.`patient_contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`patient_contact` (
  `patient_contact_id` INT(11) UNSIGNED NOT NULL,
  `patient_id` INT(11) UNSIGNED NOT NULL,
  `contact_type` VARCHAR(40) NULL DEFAULT NULL,
  `contact_details` VARCHAR(40) NULL DEFAULT NULL,
  `contact_notes` VARCHAR(80) NULL DEFAULT NULL,
  PRIMARY KEY (`patient_contact_id`),
  INDEX `patient_id` (`patient_id` ASC),
  CONSTRAINT `fk_patient_contact_patients1`
  FOREIGN KEY (`patient_id`)
  REFERENCES `medicare`.`patients` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`patient_notes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`patient_notes` (
  `patient_note_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` INT(11) UNSIGNED NOT NULL,
  `patient_note` TEXT NULL DEFAULT NULL,
  `note_type_id` INT(11) NULL DEFAULT NULL,
  `tags` TEXT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`patient_note_id`),
  INDEX `patient_id` (`patient_id` ASC),
  CONSTRAINT `fk_patient_notes_patients1`
  FOREIGN KEY (`patient_id`)
  REFERENCES `medicare`.`patients` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`referrals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`referrals` (
  `referral_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `referral_desc` VARCHAR(40) NULL DEFAULT NULL,
  `override_code` VARCHAR(10) NULL DEFAULT NULL,
  `period` VARCHAR(20) NULL DEFAULT NULL,
  `period_type` VARCHAR(20) NULL DEFAULT NULL,
  `provider` VARCHAR(45) NULL DEFAULT NULL,
  `provider_type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`referral_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`users` (
  `user_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_id` INT(11) UNSIGNED NOT NULL,
  `username` VARCHAR(80) NULL DEFAULT NULL,
  `first_name` VARCHAR(40) NULL DEFAULT NULL,
  `last_name` VARCHAR(40) NULL DEFAULT NULL,
  `email` VARCHAR(40) NULL DEFAULT NULL,
  `pw_hash` VARCHAR(40) NULL DEFAULT NULL,
  `designation` VARCHAR(40) NULL DEFAULT NULL,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `users_ok` (`username` ASC, `first_name` ASC, `last_name` ASC),
  INDEX `tenant_id` (`tenant_id` ASC),
  CONSTRAINT `fk_users_tenants1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `medicare`.`tenants` (`tenant_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `medicare`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`roles` (
  `role_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(45) NOT NULL,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`role_id`),
  UNIQUE INDEX `role_name` (`role_name` ASC, `is_active` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medicare`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`user_roles` (
  `user_id` INT(11) UNSIGNED NOT NULL,
  `role_id` INT(11) UNSIGNED NOT NULL,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  INDEX `fk_user_roles_users1_idx` (`user_id` ASC),
  INDEX `fk_user_roles_roles1_idx` (`role_id` ASC),
  UNIQUE INDEX `user_roles_ok` (`user_id` ASC, `role_id` ASC, `is_active` ASC),
  CONSTRAINT `fk_user_roles_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `medicare`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_roles_roles1`
  FOREIGN KEY (`role_id`)
  REFERENCES `medicare`.`roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medicare`.`modules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`modules` (
  `module_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module_name` VARCHAR(45) NOT NULL,
  `module_desc` VARCHAR(45) NULL,
  `is_active` TINYINT(1) UNSIGNED NULL DEFAULT 1,
  `created_by` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE INDEX `module_ok` (`is_active` ASC, `module_name` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medicare`.`roles_access`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `medicare`.`roles_access` (
  `role_access_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` INT(11) UNSIGNED NOT NULL,
  `module_id` INT(11) UNSIGNED NOT NULL,
  `write` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `read` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`role_access_id`),
  INDEX `fk_roles_access_modules1_idx` (`module_id` ASC),
  INDEX `fk_roles_access_roles1_idx` (`role_id` ASC),
  CONSTRAINT `fk_roles_access_modules1`
  FOREIGN KEY (`module_id`)
  REFERENCES `medicare`.`modules` (`module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_roles_access_roles1`
  FOREIGN KEY (`role_id`)
  REFERENCES `medicare`.`roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
