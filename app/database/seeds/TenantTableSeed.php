<?php

class TenantTableSeed extends Seeder {

	/**
	 * Run the tenant seeds.
	 *
	 * @return void
	 */
	private $num_data = 10;

	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i=1; $i <= $this->num_data ; $i++) { 
			
			$company = $faker->company;

			Tenant::create([
				'addresses_id'	     => rand(1 , 20),
				'primary_contact'  	 => $faker->phonenumber ,
				'email'  			 => $faker->email ,
				'notes'              => $faker->text
			]);

			$this->command->info('Tenants Data seeded of Data! #'.$i.'('.$company.')');
		}
		
	}

}

