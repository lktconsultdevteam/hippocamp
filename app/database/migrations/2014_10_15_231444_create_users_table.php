<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a User Table
*/

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{	
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('user_id', 36)->primary();
			$table->string('tenant_id', 36);
			$table->string('username' , 80)->unique();
			$table->string('first_name' , 80);
			$table->string('last_name' , 80);
			$table->string('email' , 40)->unique();
			$table->string('pw_hast' , 40);
			$table->string('designation' , 40)->nullable();
            $table->softDeletes();
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
			$table->timestamps();

            $table->index('tenant_id');
            $table->index('created_by');
            $table->index('updated_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
