<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admissions', function(Blueprint $table)
		{
			$table->string('admission_id', 36)->primary();
            $table->string('patient_id', 36);
            $table->date('admission_date');
            $table->date('discharge_date');
            $table->char('service_type', 5);
            $table->tinyInteger('is_paid')->default(0);
            $table->dateTime('date_payment');
			$table->timestamps();

            $table->index('patient_id');
            $table->index('admission_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admissions');
	}

}
