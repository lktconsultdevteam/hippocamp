<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function(Blueprint $table)
		{
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('address_id', 36)->primary();
			$table->string('object_id', 36);
			$table->string('object_name' , 20);
			$table->string('address1' , 40);
			$table->string('address2' , 40)->nullable();
			$table->string('address3' , 40)->nullable();
			$table->string('city' , 40);
			$table->string('state' , 40);
			$table->string('country' , 40);
			$table->tinyInteger('is_active')->default(1);
			$table->string('created_by', 36);
			$table->string('updated_by', 36);
			$table->timestamps();

            $table->index('object_id');
            $table->index('object_name');
            $table->index('created_by');
            $table->index('updated_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}
