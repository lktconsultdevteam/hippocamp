<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a Patient Contact Table
*/

class CreatePatientContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patient_contact', function(Blueprint $table)
		{
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('patient_contact_id', 36)->primary();
			$table->string('patient_id', 36);
			$table->string('contact_type' , 40);
			$table->string('contact_details' , 40);
			$table->string('note_id' , 36);

            $table->index('patient_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patient_contact');
	}

}
