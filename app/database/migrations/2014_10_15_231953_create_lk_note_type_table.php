<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a LK Note Type Table
*/

class CreateLkNoteTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lk_note_type', function(Blueprint $table)
		{	
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('lk_note_type_id', 36)->primary();
			$table->string('tenant_id', 36);
			$table->tinyInteger('is_file');
			$table->string('note_type_desc' , 40)->nullable();

            $table->index('tenant_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lk_note_type');
	}

}
