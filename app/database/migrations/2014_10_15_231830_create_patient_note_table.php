<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a Patient Note Table
*/

class CreatePatientNoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patient_note', function(Blueprint $table)
		{	
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('patient_note_id', 36)->primary();
			$table->string('patient_id');
			$table->string('note_id');
			$table->integer('note_type_id');
			$table->text('tags')->nullable();
			$table->string('created_by', 36);
			$table->timestamps();

            $table->index('patient_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patient_note');
	}

}
