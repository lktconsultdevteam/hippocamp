<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('notes', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('patient_note_id', 36)->primary();
            $table->string('object_id', 36);
            $table->string('object_name', 45);
            $table->text('notes');
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
            $table->timestamps();

            $table->index('object_id');
            $table->index('created_by');
            $table->index('updated_by');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
