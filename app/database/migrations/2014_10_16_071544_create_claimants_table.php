<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claimants', function(Blueprint $table)
		{
			$table->string('claimant_id', 36)->primary();
            $table->string('medicare_num', 10);
            $table->string('medicare_ref', 100);
            $table->string('first_name', 40);
            $table->string('last_name', 40);
            $table->string('address_id', 36);
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
			$table->timestamps();

            $table->index('medicare_num');
            $table->index('medicare_ref');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claimants');
	}

}
