<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a Patients Table
*/

class CreatePatientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patients', function(Blueprint $table)
		{
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('patient_id', 36)->primary();
            $table->string('tenant_id', 36);
			$table->string('title' , 40)->nullable();
			$table->string('first_name' , 40);
			$table->enum('gender' , ['Male' , 'Female']);
			$table->date('dob');
            $table->string('address_id', 36);
			$table->string('occupation' , 80);
			$table->string('referral_id', 36);
			$table->string('reference_no' , 40);
			$table->string('ec_name' , 40);
			$table->string('ec_phone' , 40);
			$table->string('ec_email' , 40)->nullable();
			$table->string('ec_relationship' , 40)->nullable();
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
			$table->timestamps();

            $table->index('tenant_id');
            $table->index('address_id');
            $table->index('created_by');
            $table->index('updated_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patients');
	}

}
