<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionDues extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('admission_dues', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('admission_due_id', 36)->string();
            $table->string('admission_id', 36);
            $table->float('amount');
            $table->text('description')->nullable();
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
            $table->timestamps();

            $table->index('admission_id');
            $table->index('created_by');
            $table->index('updated_by');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
