<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a LK Referrals Table
*/
class CreateLkReferralsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lk_referrals', function(Blueprint $table)
		{
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('lk_referral_id', 36)->primary();
			$table->string('tenant_id', 36);
			$table->string('referral_desc' , 40)->nullable();

            $table->index('tenant_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lk_referrals');
	}

}
