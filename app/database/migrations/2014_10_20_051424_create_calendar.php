<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendar extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('calendar', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('calender_id', 36)->primary();
            $table->string('patient_id', 36);
            $table->string('subject', 45);
            $table->time('schedule_at');
            $table->string('location', 45);
            $table->tinyInteger('is_active');
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
            $table->timestamps();
            $table->softDeletes();

            $table->index('patient_id');
            $table->index('created_by');
            $table->index('updated_by');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
