<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimantsAccountTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claimants_account', function(Blueprint $table)
		{
            $table->string('claimant_id', 36)->primary();
            $table->string('account_name', 30);
            $table->integer('account_number');
            $table->string('account_bsb', 6);
            $table->tinyInteger('is_active')->default(1);
			$table->timestamps();

           $table->unique(array('claimant_id', 'account_number', 'is_active'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claimants_account');
	}

}
