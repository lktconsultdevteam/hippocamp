<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('claims', function(Blueprint $table)
		{
			$table->string('claim_id', 36)->primary();
            $table->string('claimant_id', 36);
            $table->string('patient_id', 36);
            $table->string('claim_type', 5);
            $table->float('charge_amount');
            $table->date('date_claims');
			$table->timestamps();

            $table->index('claimant_id');
            $table->index('patient_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claims');
	}

}
