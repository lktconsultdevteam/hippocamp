<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkServiceTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('lk_service_types', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('lk_service_type_id', 36)->primary();
            $table->string('tenant_id', 36);
            $table->char('service_code', 2);
            $table->tinyInteger('is_active')->default(1);

            $table->index('tenant_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
