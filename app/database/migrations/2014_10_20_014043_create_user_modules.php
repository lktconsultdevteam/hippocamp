<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserModules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('user_modules', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->string('user_module_id', 36)->primary();
            $table->string('module_name', '45');
            $table->string('description', 100)->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
            $table->softDeletes();
            $table->timestamps();

            $table->index('module_name');
            $table->index('created_by');
            $table->index('updated_by');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
