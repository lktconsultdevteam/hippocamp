<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
* Create a Tenants Table
*/

class CreateTenantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tenants', function(Blueprint $table)
		{
			/**
			* Set the Engine type to InnoDB
			*/
			$table->engine = 'InnoDB';

			$table->string('tenant_id', 36)->primary();
            $table->string('company', 40);
			$table->string('address_id', 36);
			$table->string('primary_contact' , 40);
			$table->string('email' , 40)->unique();
			$table->string('note_id', 36);
            $table->softDeletes();
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
			$table->timestamps();

            $table->index('company');
            $table->index('address_id');
            $table->index('note_id');
            $table->index('created_by');
            $table->index('updated_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tenants');
	}

}
