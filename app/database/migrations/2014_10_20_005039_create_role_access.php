<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleAccess extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('roles_access', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('role_access_id', 36)->primary();
            $table->string('role_id', 36);
            $table->string('user_module_id', 36);
            $table->tinyInteger('write')->default(0);
            $table->tinyInteger('read')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->index('role_id');
            $table->index('user_module_id');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
