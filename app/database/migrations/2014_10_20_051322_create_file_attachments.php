<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileAttachments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('file_attachments', function(Blueprint $table)
        {
            /**
             * Set the Engine type to InnoDB
             */
            $table->engine = 'InnoDB';

            $table->string('file_attachment_id', 36)->primary();
            $table->string('attachment_id', 36);
            $table->string('file_name', 45);
            $table->string('file_size', 10);
            $table->tinyInteger('is_deleted')->default(0);
            $table->string('created_by', 36);
            $table->string('updated_by', 36);
            $table->timestamps();

            $table->index('attachment_id');
            $table->index('created_by');
            $table->index('updated_by');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
